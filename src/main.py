from PIL import Image


def main():
    image = Image.open('../images/Lenna.png')
    rotated_image = image.rotate(45)
    rotated_image.save('../images/Lenna_rotated.png')

if __name__ == '__main__':
    main()
